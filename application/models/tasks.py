from application.models import db


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.Column(db.String(120))
    assigned_to = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)
    employee = db.relationship('Employee', backref=db.backref('task'))


    def __init__(self, task, assigned_to):
        self.task = task
        self.assigned_to = assigned_to

