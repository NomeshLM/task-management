from flask import jsonify, request
from application.routes import app, token_required
from application.models import db, employees, tasks, manager
from application.utilities import serialize


@app.route('/task/add', methods=['POST'])
@token_required
def add_task():
    formdata = request.get_json(force=True)
    mydata = tasks.Task(formdata['task'], request.data.id)
    try:
        db.session.add(mydata)
        db.session.commit()
        return jsonify({'message': 'Task has been added successfully'})
    except Exception as e:
        print(e)
        return jsonify({'message': 'Task cannot be added'})


@app.route('/employee/<emp_id>/tasks')
@token_required
def get_employee_tasks_for_manager(emp_id):
    try:
        mydata = manager.EmpManager.query.filter_by(employee_id=emp_id, manager_id=request.data.id).first()
        return '%s' % serialize(mydata.employee.task)
    except Exception as e:
        print(e)
        return jsonify({'message': e})


@app.route('/task/view')
@token_required
def get_tasks():
    try:
        mydata = tasks.Task.query.filter_by(assigned_to=request.data.id).all()
        return '%s' % serialize(mydata)
    except Exception as e:
        print(e)
        return jsonify({'message': "You can't view tasks of this employee"})


@app.route('/task/update', methods=['POST'])
@token_required
def update_task():
    try:
        formdata = request.get_json(force=True)
        task_id = formdata['task_id']
        task_details = tasks.Task.query.filter_by(id=task_id).first()
        task_details.task = formdata['task']
        db.session.commit()
        return jsonify({'message': 'Task Updated Successfully'})
    except Exception as e:
        print(e)
        return jsonify({'message': "Couldn't update details"})
